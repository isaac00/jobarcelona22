import numpy as np

class OuterInnerConstantDistribution:
    """
    Models a 2-part piecewise constant distribution 
    where an inner interval lies within the outer interval.
    (So the outer interval may be split in two)

    Must be given the bounds of both intervals.

    All values are assumed to lie within the outer bounds.
    """
    def __init__(self, outer_lbound, outer_ubound, inner_lbound, inner_ubound):
        self.outer_lbound = outer_lbound
        self.outer_ubound = outer_ubound
        self.inner_lbound = inner_lbound
        self.inner_ubound = inner_ubound
    
    def fit(self, X):
        in_mask, _ = self.get_in_out_masks_(X)
        total_count = len(X)
        in_count = np.count_nonzero(in_mask)
        out_count = total_count - in_count

        inner_width = self.inner_ubound - self.inner_lbound
        outer_width = self.outer_ubound - self.outer_lbound - inner_width

        self.in_proba = (in_count / total_count) / inner_width
        self.out_proba =  (out_count / total_count) / outer_width

        self.log_in_proba = np.log(self.in_proba)
        self.log_out_proba = np.log(self.out_proba)

        return self

    def predict_proba(self, X):
        in_mask, out_mask = self.get_in_out_masks_(X)
        proba = np.empty_like(X, dtype=float)
        proba[in_mask] = self.in_proba
        proba[out_mask] = self.out_proba
        return proba

    def predict_log_proba(self, X):
        in_mask, out_mask = self.get_in_out_masks_(X)
        log_proba = np.empty_like(X, dtype=float)
        log_proba[in_mask] = self.log_in_proba
        log_proba[out_mask] = self.log_out_proba
        return log_proba

    def get_in_out_masks_(self, X):
        in_mask = np.logical_and(X >= self.inner_lbound, X < self.inner_ubound)
        out_mask = np.logical_not(in_mask)
        return in_mask, out_mask
