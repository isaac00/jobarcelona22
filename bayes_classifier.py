from sklearn.base import ClassifierMixin
import numpy as np

class BayesClassifier(ClassifierMixin):
    """
    Multi-class classifier based on applying Bayes' rule.
    Uses a user-defined model for the posterior.
    """
    def __init__(self, posterior_models):
        """
        Args:
            posterior_models: Sequence of instances of classes implementing the
                posterior models of each class. They should implement the methods
                fit and either predict_log_proba or score_samples. The latter should
                return the log probability density under some data.
        """
        self.__posterior_models = posterior_models
        self.n_classes = len(posterior_models)

    def fit(self, X, y):
        """
        Trains both the prior as well as all the posterior models on the
        given data.

        Args:
            X: array-like of shape (n_samples, n_features)
            y: array-like of shape (n_samples,) 
                Class label of each training example. The labels should be 
                integers from 0 up to the number of classes.
        """
        self.fit_priors(None, y)

        for class_, model in enumerate(self.__posterior_models):
            mask = (y == class_)
            X_single_class = X[mask]
            model.fit(X_single_class)

    def fit_priors(self, X, y):
        """
        Trains the prior on the given data.
        
        Args:
            X: Ignored, present for API consistency.
            y: array-like of shape (n_samples,) 
                Class label of each training example. The labels should be 
                integers from 0 up to the number of classes.
        """
        priors = np.empty(self.n_classes)
        total_count = len(y)
        for class_ in range(len(priors)):
            mask = (y == class_)
            class_count = np.count_nonzero(mask)
            class_prior = class_count / total_count
            priors[class_] = class_prior

        self.__priors = priors
        self.__log_priors = np.log(priors)

    def score_samples(self, X):
        """
        Returns the log probabilities of every class under 
        the given observations offset by a constant. 
        """
        log_priors = self.__log_priors
        log_posteriors = np.empty((len(X), self.n_classes))

        for class_, model in enumerate(self.__posterior_models):
            if hasattr(model, "predict_log_proba"):
                log_posterior = model.predict_log_proba(X)
            elif hasattr(model, "score_samples"):
                log_posterior = model.score_samples(X)
            log_posteriors[:, class_] = log_posterior

        return log_priors + log_posteriors

    def predict(self, X):
        """
        Predicts the most likely class under the given observations.
        """
        log_probs = self.score_samples(X)
        predictions = np.argmax(log_probs, axis=1)
        return predictions
            

