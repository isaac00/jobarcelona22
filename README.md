# JOBarcelona22

Repository for the submission of the JOBarcelona'22 data science online challenge

# Files

"EDA.ipynb" contains the exploratory data analysis.

"modelling.ipynb" creates the predictive model.

"Final_prediction.ipynb" predicts the labels of the test data

"bayes_classifier.py", "joint_distribution.py" and "piecewise_constant.py" define classes used to create the model.
