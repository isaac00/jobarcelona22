import numpy as np

class JointDistribution:
    """
    Models the joint distribution of varioius models,
    assuming conditional independence of the models.
    """
    def __init__(self):
        self.__models = []
        self.__features = []

    def add_model(self, model, features):
        """
        Add a model to the joint distribution.
        Args:
            model: An instance of a class implementing the fit method and
                either predict_log_proba or score_samples. The latter should return
                the log probability density given some data.

            features: Sequence of features the model should use for training
                and prediction.
        """
        self.__models.append(model)
        self.__features.append(features)

    def fit(self, X, y=None):
        """
        Train all the models on the given data
        
        Args:
            X: array-like of shape (n_samples, n_features)
            y: Ignored, present for API consistency.
        """
        for model, features in zip(self.__models, self.__features):
            model.fit(X[features])

    def predict_log_proba(self, X):
        """
        Predict the log probability density of the observations
        under the joint model.
        """
        log_proba = np.zeros(len(X))
        for model, features in zip(self.__models, self.__features):
            if hasattr(model, "predict_log_proba"):
                model_log_proba = model.predict_log_proba(X[features])
            elif hasattr(model, "score_samples"):
                model_log_proba = model.score_samples(X[features])
            log_proba += model_log_proba
        
        return log_proba